package reproductor;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import sun.util.logging.PlatformLogger;

public class PanelReproductor extends javax.swing.JFrame {

    long longitudTotal;
    long pausa;
    Boolean pausado = false;
    FileInputStream FIS;
    BufferedInputStream BIS;
    Player player;
    
    File miArchivo = null;
    
    public PanelReproductor() {
        initComponents();
        botonReproducir.setEnabled(false);
        botonPausar.setEnabled(false);
        botonDetener.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        botonAbrir = new javax.swing.JButton();
        botonDetener = new javax.swing.JButton();
        botonReproducir = new javax.swing.JButton();
        botonPausar = new javax.swing.JButton();
        Titulo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        botonAbrir.setText("Abrir");
        botonAbrir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                botonAbrirMouseReleased(evt);
            }
        });
        botonAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAbrirActionPerformed(evt);
            }
        });

        botonDetener.setText("Detener");
        botonDetener.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                botonDetenerMouseReleased(evt);
            }
        });
        botonDetener.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDetenerActionPerformed(evt);
            }
        });

        botonReproducir.setText("Reproducir");
        botonReproducir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                botonReproducirMouseReleased(evt);
            }
        });

        botonPausar.setText("Pausar");
        botonPausar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                botonPausarMouseReleased(evt);
            }
        });
        botonPausar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonPausarActionPerformed(evt);
            }
        });

        Titulo.setText("Titulo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Titulo)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botonAbrir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(112, 112, 112)
                        .addComponent(botonReproducir, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(botonPausar, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(botonDetener, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                        .addGap(35, 35, 35))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(Titulo)
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(botonAbrir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonReproducir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonDetener, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonPausar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonDetenerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDetenerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botonDetenerActionPerformed

    private void botonAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAbrirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botonAbrirActionPerformed

    private void botonPausarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonPausarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botonPausarActionPerformed

    private void botonAbrirMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonAbrirMouseReleased
        // TODO add your handling code here:
        botonAbrir.setEnabled(false);
        botonReproducir.setEnabled(true);
        
        JFileChooser chooser = new JFileChooser();
        
        int val = chooser.showOpenDialog(null);
        
        if(val == JFileChooser.APPROVE_OPTION){
            miArchivo = chooser.getSelectedFile();
            String cancion = miArchivo + "";
            
            String nombre = chooser.getSelectedFile().getName();
            Titulo.setText(nombre);
        }
    }//GEN-LAST:event_botonAbrirMouseReleased

    private void botonReproducirMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonReproducirMouseReleased
        // TODO add your handling code here:
        botonReproducir.setEnabled(false);
        botonPausar.setEnabled(true);
        botonDetener.setEnabled(true);
        try{
            FIS = new FileInputStream(miArchivo);
            BIS = new BufferedInputStream(FIS);
            player = new Player(BIS);
            if(pausado){
                FIS.skip(longitudTotal - pausa);
                pausado = false;
            }else{
                longitudTotal = FIS.available();
            }
            new Thread(){
                public void run(){
                    try {
                        player.play();
                    }catch (JavaLayerException ex) {
                        Logger.getLogger(PanelReproductor.class.getName()).log(Level.SEVERE,null,ex);
                    }
                }
            }.start();
        }catch (JavaLayerException ex) {
            Logger.getLogger(PanelReproductor.class.getName()).log(Level.SEVERE,null,ex);
        }catch (IOException ex) {
            Logger.getLogger(PanelReproductor.class.getName()).log(Level.SEVERE,null,ex);
        }
    }//GEN-LAST:event_botonReproducirMouseReleased

    private void botonPausarMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonPausarMouseReleased
        // TODO add your handling code here:}
        botonPausar.setEnabled(false);
        botonReproducir.setEnabled(true);
        if(player != null){
            try{
                pausa = FIS.available();
                pausado = true;
                player.close();
            }catch(IOException ex){
                Logger.getLogger(PanelReproductor.class.getName()).log(Level.SEVERE,null,ex);
            }
        }
    }//GEN-LAST:event_botonPausarMouseReleased

    private void botonDetenerMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_botonDetenerMouseReleased
        // TODO add your handling code here:
        if(player != null){
            botonAbrir.setEnabled(true);
            botonPausar.setEnabled(false);
            botonDetener.setEnabled(false);
            player.close();
            Titulo.setText(null);
        }
    }//GEN-LAST:event_botonDetenerMouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PanelReproductor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titulo;
    private javax.swing.JButton botonAbrir;
    private javax.swing.JButton botonDetener;
    private javax.swing.JButton botonPausar;
    private javax.swing.JButton botonReproducir;
    // End of variables declaration//GEN-END:variables
}
